﻿using System;
using SharpLzo.Native;

namespace SharpLzo
{
    public class Lzo
    {
        // ReSharper disable once InconsistentNaming
        public const int LZO1X_999_MEM_COMPRESS = 14 * 16384 * sizeof(short);

        private readonly byte[] _workMemory;

        public static Lzo Instance { get; } = new Lzo();
        public static uint Version => LzoNative.Instance.Version();

        static Lzo()
        {
            var result = LzoNative.Instance.Init();
            if (result != LzoResult.OK)
                throw new LzoException(result, $"Failed to initialize lzo library");
        }

        public Lzo()
            : this(new byte[LZO1X_999_MEM_COMPRESS])
        {
        }

        public Lzo(byte[] workMemory)
        {
            _workMemory = workMemory;
        }

        public byte[] Compress(byte[] data)
        {
            return Compress(CompressionMode.Lzo1x_1, data);
        }

        public byte[] Compress(CompressionMode mode, byte[] data)
        {
            var result = TryCompress(mode, data, out var outData);
            if (result != LzoResult.OK)
                throw new LzoException(result);

            return outData;
        }

        public LzoResult TryCompress(byte[] data, out byte[] outData)
        {
            return TryCompress(CompressionMode.Lzo1x_1, data, out outData, _workMemory);
        }

        public LzoResult TryCompress(CompressionMode mode, byte[] data, out byte[] outData)
        {
            return TryCompress(mode, data, out outData, _workMemory);
        }

        public byte[] Decompress(byte[] data)
        {
            return Decompress(data, data.Length * 10);
        }

        public byte[] Decompress(byte[] data, int decompressedLength)
        {
            var result = TryDecompress(data, decompressedLength, out var outData);
            if (result != LzoResult.OK)
                throw new LzoException(result);

            return outData;
        }

        public LzoResult TryDecompress(byte[] data, int decompressedLength, out byte[] outData)
        {
            return TryDecompress(data, decompressedLength, out outData, null);
        }

        public static LzoResult TryCompress(CompressionMode mode, byte[] data, out byte[] outData, byte[] workMemory)
        {
            // See LZO examples: http://www.oberhumer.com/opensource/lzo/
            var compressedLength = data.Length + data.Length / 16 + 64 + 3;
            var compressed = new byte[compressedLength];
            var result = LzoNative.Instance.Compress(mode, data, data.Length, compressed, ref compressedLength, workMemory);

            if (compressedLength <= 0)
                compressed = null;
            else if (compressedLength != compressed.Length)
                Array.Resize(ref compressed, compressedLength);

            outData = compressed;
            return result;
        }

        public static LzoResult TryDecompress(byte[] data, int decompressedLength, out byte[] outData, byte[] workMemory)
        {
            var decompressed = new byte[decompressedLength];
            var result = LzoNative.Instance.DecompressSafe(data, data.Length, decompressed, ref decompressedLength, null);

            if (decompressedLength == 0)
                decompressed = Array.Empty<byte>();
            else if (decompressedLength < 0)
                decompressed = null;
            else if (decompressedLength != decompressed.Length)
                Array.Resize(ref decompressed, decompressedLength);

            outData = decompressed;
            return result;
        }
    }
}
