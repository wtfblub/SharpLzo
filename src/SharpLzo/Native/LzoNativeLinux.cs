﻿using System;
using System.Runtime.InteropServices;

namespace SharpLzo.Native
{
    internal class LzoNativeLinux : LzoNative
    {
        private const string Library = "Lzo.so";

        [DllImport(Library, CallingConvention = CallingConvention.Cdecl)]
        private static extern LzoResult __lzo_init_v2(uint v, int s1, int s2, int s3, int s4, int s5,
            int s6, int s7, int s8, int s9);

        [DllImport(Library, CallingConvention = CallingConvention.Cdecl)]
        private static extern uint lzo_version();

        [DllImport(Library, CallingConvention = CallingConvention.Cdecl)]
        private static extern LzoResult lzo1x_1_compress(byte[] inData, int inLength, byte[] outData, ref int outLength,
            byte[] wrkmem);

        [DllImport(Library, CallingConvention = CallingConvention.Cdecl)]
        private static extern LzoResult lzo1x_999_compress(byte[] inData, int inLengthm, byte[] outData, ref int outLength,
            byte[] wrkmem);

        [DllImport(Library, CallingConvention = CallingConvention.Cdecl)]
        private static extern LzoResult lzo1x_decompress_safe(byte[] inData, int inLength, byte[] outData, ref int outLength,
            byte[] wrkmem);

        public override LzoResult Init()
        {
            return __lzo_init_v2(1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
        }

        public override uint Version()
        {
            return lzo_version();
        }

        public override LzoResult Compress(CompressionMode mode, byte[] inData, int inLength, byte[] outData, ref int outLength,
            byte[] wrkmem)
        {
            switch (mode)
            {
                case CompressionMode.Lzo1x_1:
                    return lzo1x_1_compress(inData, inLength, outData, ref outLength, wrkmem);

                case CompressionMode.Lzo1x_999:
                    return lzo1x_999_compress(inData, inLength, outData, ref outLength, wrkmem);

                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }

        public override LzoResult DecompressSafe(byte[] inData, int inLength, byte[] outData, ref int outLength, byte[] wrkmem)
        {
            return lzo1x_decompress_safe(inData, inLength, outData, ref outLength, wrkmem);
        }
    }
}
